---
name: About Kochi
---

# About Kochi

Kochi, formerly Cochin (the official name until 1996), city and major port on
the Malabar Coast of the Arabian Sea, west-central Kerala state, southwestern
India. Also the name of a former princely state, “Kochi” is sometimes used to
refer to a cluster of islands and towns, including Ernakulam, Mattancheri, Fort
Cochin, Willingdon Island, Vypin Island, and Gundu Island. The urban
agglomeration includes the localities of Trikkakara, Eloor, Kalamassery, and
Trippunithura.

<img class="img img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/1/1c/Kochi-725765.jpg" alt="A view
from marine drive, Kochi">

Image source:
https://commons.wikimedia.org/wiki/File:Kochi-725765.jpg

Kochi was an insignificant fishing village until, in the 14th century, the
backwaters of the Arabian Sea and the streams descending from the Western Ghats
to the east caused the separation of the village from the mainland, turning the
landlocked harbour into one of the safest ports on India’s southwestern coast.
The port assumed a new strategic importance and began to experience commercial
prosperity.

Called the "Queen of the Arabian Sea", Kochi was an important spice trading
centre on the west coast of India from the 14th century onward, and maintained a
trade network with Arab merchants from the pre-Islamic era. In 1505, the
Portuguese established trading ports in Cochin. There are still buildings like
the Old Harbour House from this period, some of which have been renovated. The
Kingdom of Cochin allied with the Ming Dynasty, Portuguese, and Dutch and became
a princely state of the British. Kochi ranks first in the total number of
international and domestic tourist arrivals in Kerala. Kochi is the first city
in India to have a water metro project.

A system of inland waterways running parallel to the coast provides Kochi with
cheap transportation, encouraging trade. The deepwater harbour is open
year-round, even in the monsoon season, and is served by a railway that connects
it with Ernakulam. An international airport, about 17 miles (28 km) northeast of
central Kochi, offers flights to major Indian cities including Mumbai (Bombay),
Delhi, Bengaluru (Bangalore), and Chennai (Madras) as well as to many
[international destinations](https://www.flightconnections.com/flights-from-kochi-cok).

<img class="img img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/c/c2/CIAL_T.jpg" alt="Cochin
International Airport">

Image source: https://commons.wikimedia.org/wiki/File:CIAL_T.jpg

Kochi, set among picturesque lagoons and backwaters, attracts a considerable
tourist trade. At Fort Cochin is St. Francis Church, built by the Portuguese in
1510 and reputedly the first European church on Indian soil. It was for a time
the burial place of Vasco da Gama before his remains were taken to Portugal.
Other churches as well as Hindu temples, mosques, and the historic synagogue at
Mattancheri all stand in the area. The Jewish community in Kochi was the oldest
in India, claiming to date from the 4th century ce. Almost all of its several
thousand members had emigrated to Israel by the late 20th century, however.

<img class="img img-fluid" src="https://upload.wikimedia.org/wikipedia/commons/9/95/Kochi_International_Marina%2C_Bolgatty_Island%2C_Kerala%2C_India.jpg" alt="Kochi International Marina">

Kochi International Marina, India's first world-class marina, built in 2010 at
the Bolgatty Island in Kochi, Kerala.

Image source:
https://commons.wikimedia.org/wiki/File:Kochi_International_Marina,_Bolgatty_Island,_Kerala,_India.jpg

Kochi is known as the financial, commercial and industrial capital of Kerala.
It has the highest GDP as well as the highest GDP per capita in the state.
Kochi is home for the High Court of Kerala and Lakshadweep.

Kochi has been hosting India's first art biennale, the Kochi-Muziris Biennale,
since 2012, which attracts international artists and tourists.

Lesbian, gay, bisexual and transgender (LGBT) people in Kerala face legal and
social difficulties not experienced by non-LGBT persons. However, Kerala has
been at the forefront of LGBT issues in India after Tamil Nadu. It became one of
the first states in India to establish a welfare policy for the transgender
community and in 2016, proposed implementing free gender affirmation surgery
through government hospitals. However, there is rising homophobic sentiment in
the state recently due to various anti-LGBT campaigns.
