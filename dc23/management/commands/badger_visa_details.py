# -*- coding: utf-8 -*-
from django.core.mail import EmailMultiAlternatives
from django.core.management.base import BaseCommand
from django.template import engines

from register.models import Attendee, Visa


SUBJECT = "[URGENT]: DebConf 23 Conference Visa requires action by August 1"

TEMPLATE = """\
Dear {{ name }},

Apologies for the late notice. In order to issue Conference Visas for DebConf,
we will need political clearance from Ministry External Affairs (MEA), India.
This requires us to send the list of participants who need visa with their
passport numbers to the MEA. This approval could take up to two weeks, so we
need your details by 1 August.

Even if your bursary / registration is not approved yet, if you applied and
need a visa, please send to us ASAP:

1. Your full name (as it appears on the passport)
2. Passport number and
3. Passport country of issue

We will also need other details [1] but these are less urgent.

[1] https://debconf23.debconf.org/about/visas/#conference-visa

If we haven't received your passport details by 1 August, you won't be included
in the list of attendees approved for Conference visas. Your only option would
be to come under another type of visa.

Thank You

The DebConf 23 Visa Team
"""


class Command(BaseCommand):
    help = 'Send visa detail reminder emails'

    def add_arguments(self, parser):
        parser.add_argument('--yes', action='store_true',
                            help='Actually do something')
        parser.add_argument('--username',
                            help='Send mail to a specific user, only')
        parser.add_argument('--skip-users',
                            help='Skip these comma-separated usernames')

    def badger(self, attendee, dry_run):
        name = attendee.user.userprofile.display_name()
        to = attendee.user.email

        ctx = {
            'name': name,
        }

        template = engines['django'].from_string(TEMPLATE)
        body = template.render(ctx)

        if dry_run:
            print('Would badger %s <%s>' % (name, to))
            return

        email_message = EmailMultiAlternatives(
            SUBJECT, body, to=["%s <%s>" % (name, to)], from_email='visa@debconf.org')
        email_message.send()

    def handle(self, *args, **options):
        dry_run = not options['yes']
        if dry_run:
            print('Not actually doing anything without --yes')

        queryset = Attendee.objects.all()
        if options['username']:
            queryset = queryset.filter(user__username=options['username'])
        if options['skip_users']:
            skip_users = [
                username.strip()
                for username
                in options['skip_users'].split(',')
                if username.strip()]
            queryset = queryset.exclude(user__username__in=skip_users)

        for attendee in queryset:
            try:
                attendee.visa
            except Visa.DoesNotExist:
                continue
            if attendee.visa:
                self.badger(attendee, dry_run)
